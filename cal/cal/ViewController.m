//
//  ViewController.m
//  cal
//
//  Created by Clicklabs14 on 9/17/15.
//  Copyright (c) 2015 click lab. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

UIButton *button0;
UIButton *button1;
UIButton *button2;
UIButton *button3;
UIButton *button4;
UIButton *button5;
UIButton *button6;
UIButton *button7;
UIButton *button8;
UIButton *button9;
UIButton *buttonac;
UIButton *buttonaddsub;
UIButton *buttonpercent;
UIButton *buttonadd;
UIButton *buttonsub;
UIButton *buttonmul;
UIButton *buttondiv;
UIButton *buttoneq;
UIButton *buttondot;
UILabel *label;
float firstval;
float secondval;
float currentval;
float a;
NSString *str;
NSString *op=@"";
int flag = 0;
int isDot = 0;
bool isOperation = NO;
bool isTypingNumbers = NO;
int optype;
NSString *operand;
NSString *previousvalue;
bool decimalflag= YES;
- (void)viewDidLoad {
    [super viewDidLoad];
    
   // firstval=0;
    firstval= -1;
    previousvalue=@"";
    secondval=-1;
    currentval=-1;
    
        label = [[UILabel alloc] init];
        [label setFrame:CGRectMake(0,0,375,190)];
        label.backgroundColor=[UIColor darkGrayColor];
        label.textColor=[UIColor whiteColor];
        label.font=[UIFont fontWithName:@"arial" size:70];
        [self.view addSubview:label];
        label.text= @"";
        label.textAlignment= NSTextAlignmentRight;
        
        
     button0 = [[UIButton alloc]initWithFrame:CGRectMake(0,572,191,95)];
    [button0 setTitle:@"0" forState:UIControlStateNormal];
    button0.backgroundColor=[UIColor lightGrayColor];
    [button0 setTitleColor: [UIColor blackColor] forState:(UIControlStateNormal)];
    button0.tag= 0;
    button0.titleLabel.font=[UIFont fontWithName:@"arial" size:26];
    [button0 addTarget:self action: @selector(press:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button0];
    
    
    button1 = [[UIButton alloc]initWithFrame:CGRectMake(0,476,95,95)];
    [button1 setTitle:@"1" forState:UIControlStateNormal];
    button1.backgroundColor=[UIColor lightGrayColor];
    [button1 setTitleColor: [UIColor blackColor] forState:(UIControlStateNormal)];
    button1.tag= 1;
    button1.titleLabel.font=[UIFont fontWithName:@"arial" size:26];
    [button1 addTarget:self action: @selector(press:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button1];
    
    button2 = [[UIButton alloc]initWithFrame:CGRectMake(96,476,95,95)];
    [button2 setTitle:@"2" forState:UIControlStateNormal];
    button2.backgroundColor=[UIColor lightGrayColor];
    [button2 setTitleColor: [UIColor blackColor] forState:(UIControlStateNormal)];
    button2.tag= 2;
    button2.titleLabel.font=[UIFont fontWithName:@"arial" size:26];
    [button2 addTarget:self action: @selector(press:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button2];
    
    button3 = [[UIButton alloc]initWithFrame:CGRectMake(192,476,95,95)];
    [button3 setTitle:@"3" forState:UIControlStateNormal];
    button3.backgroundColor=[UIColor lightGrayColor];
    [button3 setTitleColor: [UIColor blackColor] forState:(UIControlStateNormal)];
    button3.tag= 3;
    button3.titleLabel.font=[UIFont fontWithName:@"arial" size:26];
    [button3 addTarget:self action: @selector(press:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button3];
    
    button4 = [[UIButton alloc]initWithFrame:CGRectMake(0,380,95,95)];
    [button4 setTitle:@"4" forState:UIControlStateNormal];
    button4.backgroundColor=[UIColor lightGrayColor];
    [button4 setTitleColor: [UIColor blackColor] forState:(UIControlStateNormal)];
    button4.titleLabel.font=[UIFont fontWithName:@"arial" size:26];
    button4.tag= 4;
    [button4 addTarget:self action: @selector(press:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button4];
    
    button5 = [[UIButton alloc]initWithFrame:CGRectMake(96,380,95,95)];
    [button5 setTitle:@"5" forState:UIControlStateNormal];
    button5.backgroundColor=[UIColor lightGrayColor];
    [button5 setTitleColor: [UIColor blackColor] forState:(UIControlStateNormal)];
    button5.titleLabel.font=[UIFont fontWithName:@"arial" size:26];
    button5.tag= 5;
    [button5 addTarget:self action: @selector(press:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button5];
    
    button6 = [[UIButton alloc]initWithFrame:CGRectMake(192,380,95,95)];
    [button6 setTitle:@"6" forState:UIControlStateNormal];
    button6.backgroundColor=[UIColor lightGrayColor];
    [button6 setTitleColor: [UIColor blackColor] forState:(UIControlStateNormal)];
    button6.titleLabel.font=[UIFont fontWithName:@"arial" size:26];
    button6.tag= 6;
    [button6 addTarget:self action: @selector(press:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button6];
    
    button7 = [[UIButton alloc]initWithFrame:CGRectMake(0,284,95,95)];
    [button7 setTitle:@"7" forState:UIControlStateNormal];
    button7.backgroundColor=[UIColor lightGrayColor];
    button7.tag= 7;
    [button7 setTitleColor: [UIColor blackColor] forState:(UIControlStateNormal)];
    button7.titleLabel.font=[UIFont fontWithName:@"arial" size:26];
    [button7 addTarget:self action: @selector(press:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button7];
    
    button8 = [[UIButton alloc]initWithFrame:CGRectMake(96,284,95,95)];
    [button8 setTitle:@"8" forState:UIControlStateNormal];
    button8.backgroundColor=[UIColor lightGrayColor];
    [button8 setTitleColor: [UIColor blackColor] forState:(UIControlStateNormal)];
    button8.tag= 8;
    button8.titleLabel.font=[UIFont fontWithName:@"arial" size:26];
    [button8 addTarget:self action: @selector(press:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button8];
    
    button9 = [[UIButton alloc]initWithFrame:CGRectMake(192,284,95,95)];
    [button9 setTitle:@"9" forState:UIControlStateNormal];
    button9.backgroundColor=[UIColor lightGrayColor];
    [button9 setTitleColor: [UIColor blackColor] forState:(UIControlStateNormal)];
    button9.tag= 9;
    button9.titleLabel.font=[UIFont fontWithName:@"arial" size:26];
    [button9 addTarget:self action: @selector(press:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button9];
    
    
    
    buttonac = [[UIButton alloc]initWithFrame:CGRectMake(0,188,95,95)];
    [buttonac setTitle:@"C" forState:UIControlStateNormal];
    buttonac.tag=13;
    buttonac.backgroundColor=[UIColor lightGrayColor];
    [buttonac setTitleColor: [UIColor blackColor] forState:(UIControlStateNormal)];
    buttonac.titleLabel.font=[UIFont fontWithName:@"arial" size:26];
    [buttonac addTarget:self action: @selector(buttonac:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:buttonac];
    

    
    

    buttonadd = [[UIButton alloc]initWithFrame:CGRectMake(288,476,95,95)];
    [buttonadd setTitle:@"+" forState:UIControlStateNormal];
    buttonadd.backgroundColor=[UIColor orangeColor];
    buttonadd.tag= 20;
    buttonadd.titleLabel.font=[UIFont fontWithName:@"arial" size:36];
    [buttonadd addTarget:self action: @selector(buttonaction:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:buttonadd];
    
    
    buttonsub = [[UIButton alloc]initWithFrame:CGRectMake(288,380,95,95)];
    [buttonsub setTitle:@"-" forState:UIControlStateNormal];
    buttonsub.backgroundColor=[UIColor orangeColor];
    buttonsub.titleLabel.font=[UIFont fontWithName:@"arial" size:36];
    buttonsub.tag= 21;
    [buttonsub addTarget:self action: @selector(buttonaction:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:buttonsub];
    
    
    buttondiv = [[UIButton alloc]initWithFrame:CGRectMake(288,188,95,95)];
    [buttondiv setTitle:@"/" forState:UIControlStateNormal];
    buttondiv.backgroundColor=[UIColor orangeColor];
    buttondiv.tag= 22;
    buttondiv.titleLabel.font=[UIFont fontWithName:@"arial" size:36];
    [buttondiv addTarget:self action: @selector(buttonaction:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:buttondiv];
    
    buttonmul = [[UIButton alloc]initWithFrame:CGRectMake(288,284,95,95)];
    [buttonmul setTitle:@"x" forState:UIControlStateNormal];
    buttonmul.backgroundColor=[UIColor orangeColor];
    buttonmul.tag= 23;
    buttonmul.titleLabel.font=[UIFont fontWithName:@"arial" size:36];
    [buttonmul addTarget:self action: @selector(buttonaction:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:buttonmul];
    

        buttonaddsub = [[UIButton alloc]initWithFrame:CGRectMake(96,188,95,95)];
        [buttonaddsub setTitle:@"+/-" forState:UIControlStateNormal];
        buttonaddsub.backgroundColor=[UIColor lightGrayColor];
        [buttonaddsub setTitleColor: [UIColor blackColor] forState:(UIControlStateNormal)];
        buttonaddsub.titleLabel.font=[UIFont fontWithName:@"arial" size:36];
        [buttonaddsub addTarget:self action: @selector(buttonaction:)forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:buttonaddsub];
        
        buttonpercent = [[UIButton alloc]initWithFrame:CGRectMake(192,188,95,95)];
        [buttonpercent setTitle:@"%" forState:UIControlStateNormal];
        buttonpercent.backgroundColor=[UIColor lightGrayColor];
        [buttonpercent setTitleColor: [UIColor blackColor] forState:(UIControlStateNormal)];
        buttonpercent.titleLabel.font=[UIFont fontWithName:@"arial" size:26];
        [buttonpercent addTarget:self action: @selector(buttonpercent:)forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:buttonpercent];
    
        
    buttondot = [[UIButton alloc]initWithFrame:CGRectMake(192,572,95,95)];
        [buttondot setTitle:@"." forState:UIControlStateNormal];
        buttondot.backgroundColor=[UIColor lightGrayColor];
        buttondot.tag=12;
        [buttondot setTitleColor: [UIColor blackColor] forState:(UIControlStateNormal)];
        buttondot.titleLabel.font=[UIFont fontWithName:@"arial" size:36];
        [buttondot addTarget:self action: @selector(pressdot:)forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:buttondot];
        
    buttoneq = [[UIButton alloc]initWithFrame:CGRectMake(288,572,95,95)];
        [buttoneq setTitle:@"=" forState:UIControlStateNormal];
        buttoneq.backgroundColor=[UIColor orangeColor];
        buttoneq.titleLabel.font=[UIFont fontWithName:@"arial" size:36];
        [buttoneq addTarget:self action: @selector(presseq:)forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:buttoneq];
        
    }

    -(void)buttonac:(UIButton *) sender{
        if ([sender tag]==13) {
            firstval= -1;
            label.text =@"";
            previousvalue=@"";
            secondval=-1;
            currentval=-1;
        }
    }
-(void)buttonaction:(UIButton *) sender{
    if ([sender tag]==20){
        optype=1;
        if (firstval == -1) {
           
        firstval = [label.text floatValue];
        label.text = @"";
    }
        else if(([label.text length ]> 0)&&(firstval >0)){
          secondval =[label.text floatValue] ;
            secondval= firstval+ secondval;
            label.text=[NSString stringWithFormat:@"%.1f",secondval];
            firstval=secondval;
             label.text = @"";
            
            
        }
    }
    else if ([sender tag]==21){
        optype=2;
        if (firstval == -1){
        firstval = [label.text floatValue];
        label.text = @"";
        }
        else if(([label.text length ]> 0)&&(firstval >0)){
            secondval =[label.text floatValue] ;
            secondval= firstval- secondval;
            label.text=[NSString stringWithFormat:@"%.1f",secondval];
            firstval=secondval;
            label.text = @"";
        }
    }
    
    else if ([sender tag]==22){
        optype=4;
        if (firstval == -1){
            firstval = [label.text floatValue];
            label.text = @"";
        }
        else if(([label.text length ]> 0)&&(firstval >0)){
            secondval =[label.text floatValue] ;
            secondval= firstval/ secondval;
            label.text=[NSString stringWithFormat:@"%.1f",secondval];
            firstval=secondval;
            label.text = @"";
        }
    }
    else if ([sender tag]==23){
        optype=3;
        if (firstval == -1){
            firstval = [label.text floatValue];
            label.text = @"";
        }
        else if(([label.text length ]> 0)&&(firstval >0)){
            secondval =[label.text floatValue] ;
            secondval= firstval*secondval;
            label.text=[NSString stringWithFormat:@"%.1f",secondval];
            firstval=secondval;
            label.text = @"";
        }
    }
    
    
}

-(void)pressdot: (UIButton*) sender{
    
/*if(decimalflag)
{
    label.text=[label.text stringByAppendingString:sender.titleLabel.text];
}
decimalflag=NO;
}*/    if([label.text isEqualToString: nil]){
        [label setText:@"0."];
    }
    else{
        NSString *value = label.text;
        if([value rangeOfString:@".1"].location == NSNotFound){
            label.text = [label.text stringByAppendingString:@"."];
        }
    }
}
-(void)presseq: (UIButton *)sender{
        
        /*int newval = [label.text intValue];
        newval = first + newval;
        label.text = [ NSString stringWithFormat:@"%d",newval];
        
        
        int currentValue = [label.text intValue];
        
        if ( newval != 0 && currentValue != 0)
        {
            if (sender.tag == 10 )
                newval += currentValue;
            else if ([operand isEqualToString:@"-"])
                newval -= currentValue;
            else if ([operand isEqualToString:@"×"])
                newval *= currentValue;
            else if ([operand isEqualToString:@"/"])
                newval /= currentValue;
        }
        
        else{
            newval = currentValue;
        
        label.text = [NSString stringWithFormat:@"%d", newval];
        }
        if ([sender tag]== 10) {
            if (firstval == -1) {
                firstval = [label.text floatValue];
                label.text =@"";
            }
        }
        else if (([label.text length]>0) && (firstval>0))
        {
            secondval = [label.text floatValue];
            currentval = firstval + secondval;
            label.text = [NSString stringWithFormat:@"%f", currentval];
            firstval = currentval;
            label.text = @"";
    }*/
     [buttoneq setEnabled:NO];
        float a =[label.text floatValue];
        if (optype==0){
            a=firstval;
        }
        else{
        switch (optype) {
            case 1:
                [buttoneq setEnabled:YES];
                a= firstval + a;
                label.text = [NSString stringWithFormat:@"%.1f",a];
                break;
            case 2:
                [buttoneq setEnabled:YES];
                a= firstval - a;
                label.text = [NSString stringWithFormat:@"%.1f",a];
                break;
            case 3:
                [buttoneq setEnabled:YES];
                a= firstval * a;
                label.text = [NSString stringWithFormat:@"%.1f",a];
                break;
            case 4:
                [buttoneq setEnabled:YES];
                a= firstval / a;
                label.text = [NSString stringWithFormat:@"%.1f",a];
                break;
            default:
                break;
        }
        }
            
    }

    
    -(void) press: (UIButton *)sender{
        if ([sender tag] == 0)
        {
            if ([label.text length]> 0) {
                NSString *previousvalue= label.text;
                label.text = [NSString stringWithFormat:@"%@%ld",previousvalue,[sender tag]];
            }
            else
                label.text= @"0";
        }
        else if ([sender tag] == 1)
        {
            if ([label.text length]> 0) {
                NSString *previousvalue= label.text;
                label.text = [NSString stringWithFormat:@"%@%ld",previousvalue,[sender tag]];
            }
            else {
                label.text= @"1";
            }
        }
        else if ([sender tag] == 2)
        {
            if ([label.text length]> 0) {
                NSString *previousvalue= label.text;
                label.text = [NSString stringWithFormat:@"%@%ld",previousvalue,[sender tag]];
            }
            else{
                label.text= @"2";
            }
        }
        else if ([sender tag] == 3)
        {
            if ([label.text length]> 0) {
                NSString *previousvalue= label.text;
                label.text = [NSString stringWithFormat:@"%@%ld",previousvalue,[sender tag]];
            }
            else{
                label.text= @"3";
            }
        }
        else if ([sender tag] == 4)
        {
            if ([label.text length]> 0) {
                NSString *previousvalue= label.text;
                label.text = [NSString stringWithFormat:@"%@%ld",previousvalue,[sender tag]];
            }
            else{
                label.text= @"4";
            }
        }
        else if ([sender tag] == 5)
        {
            if ([label.text length]> 0) {
                NSString *previousvalue= label.text;
                label.text = [NSString stringWithFormat:@"%@%ld",previousvalue,[sender tag]];
            }
            else{
                label.text= @"5";
            }
        }
        else if ([sender tag] == 6)
        {
            if ([label.text length]> 0) {
                NSString *previousvalue= label.text;
                label.text = [NSString stringWithFormat:@"%@%ld",previousvalue,[sender tag]];
            }
            else{
                label.text= @"6";
            }
        }
        else if ([sender tag] == 7)
        {
            if ([label.text length]> 0) {
                NSString *previousvalue= label.text;
                label.text = [NSString stringWithFormat:@"%@%ld",previousvalue,[sender tag]];
            }
            else{
                label.text= @"7";
            }
        }
        else if ([sender tag] == 8)
        {
            if ([label.text length]> 0) {
                NSString *previousvalue= label.text;
                label.text = [NSString stringWithFormat:@"%@%ld",previousvalue,[sender tag]];
            }
            else{
                label.text= @"8";
            }
        }
        else if ([sender tag] == 9)
        {
            if ([label.text length]> 0) {
                NSString *previousvalue= label.text;
                label.text = [NSString stringWithFormat:@"%@%ld",previousvalue,[sender tag]];
            }
            else{
                label.text= @"9";
            }
        }
       /* else if ([sender tag] == 12)
        {
            if ([label.text length]> 0) {
                NSString *previousvalue= label.text;
                label.text = [NSString stringWithFormat:@"%@%ld",previousvalue,[sender tag]];
            }
            else{
                label.text= @".";
            }
        }*/
    }
    // Do any additional setup after loading the view, typically from a nib.

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
